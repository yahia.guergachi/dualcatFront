import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetectionHttpServiceService {

  constructor(private httpClient: HttpClient) { }

  launchDetection(count: any): Observable<any[]> {
    return forkJoin(Array.from({ length: count }, (_, i) => i).map(i => {
      console.log(i)
      return this.httpClient.post("http://localhost:3000/detect", { "count": i })
    })
    )
  }
}
