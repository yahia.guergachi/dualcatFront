import { Component, OnInit, ViewChild } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DetectionHttpServiceService } from '../services/detection/detection-http-service.service';

export interface DetectionData {
  url: string;
  score: number;
  label: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})


export class DashboardComponent implements OnInit {



  public graph = {
    data: [{
      values: [0, 0],
      labels: ["Détecté", "Non Détecté"],
      type: 'pie'
    }],
    layout: {
      height: 400,
      width: 500


    }
  };

  winnerUrl = "";
  numberOfCats = 0;
  isLoading = false;

  displayedColumns: string[] = ['url', 'score', 'label'];
  dataSource: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  stats: any = [{ url: "a", score: 0, label: "aa" }, { url: "a", score: 2, label: "aa" }];
  mode: ProgressSpinnerMode = 'determinate';
  color: ThemePalette = 'primary';
  value = 50;

  constructor(private detection: DetectionHttpServiceService) { }

  async ngOnInit() {
    var initData: any = []
    await this.launchDetection();
  }

  async launchDetection() {
    this.isLoading = true;
    console.log(this.isLoading)
    this.dataSource = new MatTableDataSource(this.stats);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.stats = await this.detection.launchDetection(this.numberOfCats).toPromise();
    if (!this.stats) {
      this.stats = [{ url: "a", score: 0, label: "aa" }, { url: "a", score: 2, label: "aa" }];
    }

    this.stats.sort(function (a: any, b: any) {
      return - a.score + b.score;
    })

    this.dataSource = new MatTableDataSource(this.stats);

    this.graph.data[0].values = [this.stats.filter((e: any) => { return e.score > 0 }).length, this.stats.filter((e: any) => { return e.score == 0 }).length]
    console.log('==>', this.dataSource, this.stats)

    this.isLoading = false;
    console.log(this.isLoading)


  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
